package com.brockhaus.SuperDuperMarkt.controller;

import com.brockhaus.SuperDuperMarkt.models.Cheese;
import com.brockhaus.SuperDuperMarkt.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"api/product"})
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping(value = {"get"})
    public ResponseEntity<?> getAllProduct(){
        try{
            return new ResponseEntity<>(productService.getAllProduct(), new HttpHeaders(), HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<String>(exception.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

}
