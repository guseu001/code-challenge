package com.brockhaus.SuperDuperMarkt.models;

import java.time.LocalDate;

public class Product {
    protected String name;
    protected int quality;
    protected LocalDate expiryDate;
    protected LocalDate createdProductDate;
    protected double basePrice;
    protected double dayPrice;

    public Product(){}

    public Product(String name, int quality, LocalDate expiryDate, double price) {
        this.name = name;
        this.quality = quality;
        this.expiryDate = expiryDate;
        this.basePrice = price;
        this.createdProductDate = LocalDate.now();
        this.dayPrice = calculateDayPrice(this.basePrice, this.quality);
    }

    /**
     * return the Price of the Day
     * for the Formula BasePrice + 0.10 * quality
     * @param quality
     * @return
     */
    public double calculateDayPrice(double basePrice, int quality){
        return basePrice + (0.10 * quality);
    }

    /**
     * return true if Quality better than 0
     * @param quality
     * @return
     */
    public boolean isQualityValid(int quality){
        return quality > 0;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public LocalDate getCreatedProductDate() {
        return createdProductDate;
    }

    public void setCreatedProductDate(LocalDate createdProductDate) {
        this.createdProductDate = createdProductDate;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getDayPrice() {
        return dayPrice;
    }

    public void setDayPrice(double dayPrice) {
        this.dayPrice = dayPrice;
    }

    public String toString() {
        return String.format("Name: %s, Price: %.2f, Quality: %d, ExpiryDate: %s",
                name, basePrice, quality, expiryDate );
    }
}
