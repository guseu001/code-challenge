package com.brockhaus.SuperDuperMarkt.models;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Wine extends Product{
    private LocalDate startDate;

    public Wine(String name, int quality, double price){
        super();
        this.name = name;
        this.quality = quality;
        this.basePrice = price;
    }

    /**
     * return true if Quality are higher than 0
     * @param quality
     * @return
     */
    @Override
    public boolean isQualityValid(int quality){
        return quality >= 0;
    }

    public void updateQuality(){
        long daysSinceStart = ChronoUnit.DAYS.between(startDate, LocalDate.now());
        if (daysSinceStart % 10 == 0 && quality < 50) {
            quality++;
        }
    }
}
