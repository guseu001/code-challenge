package com.brockhaus.SuperDuperMarkt.models;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public class Cheese extends Product{

    private final int VALID_QUALITY_LEVEL_FOR_CHEESE = 30;
    private LocalDate memoryLastDateForUpdateQuality;
    private int quality;
    private double dayPrice;

    public Cheese(String name, int quality, double price){
        super(name, quality, calculateExpiryDate(), price);
        this.quality = quality;
        this.expiryDate = calculateExpiryDate();
        this.memoryLastDateForUpdateQuality = createdProductDate;
    }

    /**
     * return true if higher than 30
     * @param quality
     * @return
     */
    @Override
    public boolean isQualityValid(int quality){
        return quality >= VALID_QUALITY_LEVEL_FOR_CHEESE;
    }


    /**
     * return a Random LocalDate between 50 - 100 days in the future
     * @return
     */
    public static LocalDate calculateExpiryDate(){
        int daysUntilExpiry = ThreadLocalRandom.current().nextInt(50, 100);
        return LocalDate.now().plusDays(daysUntilExpiry);
    }

    public void updateQuality(){
        if(this.memoryLastDateForUpdateQuality.getDayOfMonth() < LocalDate.now().getDayOfMonth()){
            this.memoryLastDateForUpdateQuality = LocalDate.now();
            this.quality--;
        }
    }
}
