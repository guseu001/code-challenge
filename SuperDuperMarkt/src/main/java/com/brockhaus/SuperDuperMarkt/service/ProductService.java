package com.brockhaus.SuperDuperMarkt.service;

import com.brockhaus.SuperDuperMarkt.models.Product;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ProductService {

    private Set<Product> listOfProduct = new HashSet<Product>();

    public void addProduct(Product product){
        listOfProduct.add(product);
    }

    public void deleteProduct(String name){
        listOfProduct.removeIf(product -> product.getName().equals(name));
    }

    public Set<Product> getAllProduct(){
        return listOfProduct;
    }
}
