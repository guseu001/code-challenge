package com.brockhaus.SuperDuperMarkt;

import com.brockhaus.SuperDuperMarkt.models.Cheese;
import com.brockhaus.SuperDuperMarkt.models.Product;
import com.brockhaus.SuperDuperMarkt.models.Wine;
import com.brockhaus.SuperDuperMarkt.service.ProductService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Scanner;

@Component
public class ProductCommandLineRunner implements CommandLineRunner {
    ProductService productService = new ProductService();

    @Override
    public void run(String... args) throws Exception {
        boolean isAdd = true;
        do {
            Scanner scan = new Scanner(System.in);
            System.out.println("New Produkt hinzufuegen ? ");
            System.out.println("Gibt der Product Name ein ? ");
            String name = scan.nextLine();
            System.out.println("Gibt der Product Quality ein ? ");
            int quality = scan.nextInt();
            System.out.println("Gibt der Product Preis ein ? ");
            double basePreis = scan.nextDouble();

            System.out.print("Is diese Product ein Kaese, ein Wine oder andere Product ?\n" +
                    "Kaese = 1, Wine = 2 und andere = 0");
            int index = scan.nextInt();

            Product product;
            switch (index){
                case 1 : product = new Cheese(name, quality, basePreis);
                    break;
                case 2 : product = new Wine(name, quality, basePreis);
                    break;
                default: System.out.println("Keine Besondere Merkmel erkannt. Product wurde deine Sortiments hinzugefuegt");
                    product = new Product(name, quality, LocalDate.now(), basePreis);
                    break;
            }
            productService.addProduct(product);
            System.out.println("Product into the list hinzugefuegt!");

            System.out.println("Weitere Produkte einpflegen JA = true, NEIN = false?");
            isAdd = scan.nextBoolean();
        }
        while (isAdd);
        System.out.println("Alle Produkte wurde erfasst!");
    }
}
