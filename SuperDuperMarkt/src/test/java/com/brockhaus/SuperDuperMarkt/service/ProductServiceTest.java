package com.brockhaus.SuperDuperMarkt.service;

import com.brockhaus.SuperDuperMarkt.models.Cheese;
import com.brockhaus.SuperDuperMarkt.models.Product;
import com.brockhaus.SuperDuperMarkt.models.Wine;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ProductServiceTest {

    ProductService productService = new ProductService();

    @Test
    void addProduct() {
        Product product = new Product("Test Product", 30, LocalDate.now(), 5.00);
        productService.addProduct(product);
        Cheese cheese = new Cheese("Test Cheese", 39, 15.00);
        productService.addProduct(cheese);
        Wine wine = new Wine("Test Wine", -1, 35.00);
        productService.addProduct(wine);

        assertEquals(3, productService.getAllProduct().size());
    }

}